package com.gusrubin.apiresthelloworld.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
public class HelloWorldController {
	
	private static final String servicePath = "/hello";
	private static final String contentType = "application/text";
	
	@RequestMapping(path = servicePath, method = RequestMethod.GET, produces = contentType)
    public ResponseEntity<?> getHelloWorld(UriComponentsBuilder uriBuilder) {
        
		String responseBody = "Hello World!";

        return ResponseEntity.ok(responseBody);
    }

}