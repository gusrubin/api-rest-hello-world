package com.gusrubin.apiresthelloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.gusrubin.apiresthelloworld")
public class ApiRestHelloWorldApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRestHelloWorldApplication.class, args);
	}

}
